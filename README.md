# PHP for Drupal in development environment.

[![pipeline status](https://gitlab.com/florenttorregrosa-docker/images/docker-drupal-php-dev/badges/develop/pipeline.svg)](https://gitlab.com/florenttorregrosa-docker/images/docker-drupal-php-dev/-/commits/develop)

This repo contains the build for the maintained docker container https://hub.docker.com/r/florenttorregrosa/drupal-php-dev.
